package com.novel.system.controller;

import com.novel.common.constants.UserConstants;
import com.novel.common.exception.business.BusinessException;
import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.utils.excel.ExcelUtils;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.system.domain.SysConfig;
import com.novel.system.service.SysConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 参数配置 信息操作处理
 *
 * @author novel
 * @date 2020/07/08
 */
@RestController
@RequestMapping("/system/config")
public class SysConfigController extends BaseController {

    private final SysConfigService sysConfigService;

    public SysConfigController(SysConfigService sysConfigService) {
        this.sysConfigService = sysConfigService;
    }

    /**
     * 查询参数配置列表
     */
    @RequiresPermissions("system:config:list")
    @GetMapping("/list")
    public TableDataInfo list(SysConfig config) {
        startPage();
        return getDataTable(sysConfigService.selectConfigList(config));
    }


    /**
     * 新增参数配置
     *
     * @param config 参数配置
     * @return 操作结果
     */
    @Log(title = "参数管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:config:add")
    @PostMapping("/add")
    public Result addSave(@Validated(AddGroup.class) SysConfig config) {
        if (UserConstants.CONFIG_KEY_NOT_UNIQUE.equals(sysConfigService.checkConfigKeyUnique(config))) {
            throw new BusinessException("新增参数'" + config.getConfigName() + "'失败，参数键名已存在");
        }
        config.setCreateBy(getUserName());
        return toAjax(sysConfigService.insertConfig(config), "新增参数成功", "新增参数失败");
    }

    /**
     * 保存编辑参数配置
     *
     * @param config 参数配置
     * @return 操作结果
     */
    @Log(title = "参数管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:config:edit")
    @PutMapping("/edit")
    public Result editSave(@Validated(EditGroup.class) SysConfig config) {
        if (UserConstants.CONFIG_KEY_NOT_UNIQUE.equals(sysConfigService.checkConfigKeyUnique(config))) {
            throw new BusinessException("修改参数'" + config.getConfigName() + "'失败，参数键名已存在");
        }
        config.setUpdateBy(getUserName());
        return toAjax(sysConfigService.updateConfig(config), "修改参数成功！", "修改参数失败");
    }

    /**
     * 删除参数配置
     *
     * @param ids 参数配置ID
     * @return 操作结果
     */
    @Log(title = "参数管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:config:remove")
    @DeleteMapping("/remove")
    public Result remove(Long[] ids) {
        return toAjax(sysConfigService.batchDeleteConfig(ids), "删除参数成功", "删除参数失败");
    }


    /**
     * 校验参数键是否唯一
     *
     * @param config 包含id和key
     * @return 结果
     */
    @PostMapping("/checkConfigKeyUnique")
    public Result checkConfigKeyUnique(SysConfig config) {
        return toAjax(sysConfigService.checkConfigKeyUnique(config));
    }


    /**
     * 导出参数配置
     *
     * @param config 查询条件
     * @return 结果
     */
    @Log(title = "参数管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:config:export")
    @GetMapping("/export")
    public Result export(SysConfig config) {
        startPage();
        List<SysConfig> list = sysConfigService.selectConfigList(config);
        String fileName = ExcelUtils.exportExcelToFile(list, "参数管理", SysConfig.class);
        Map<String, Object> map = new HashMap<>(1);
        map.put("fileName", fileName);
        return toAjax(map);
    }

    /**
     * 根据参数键名查询参数值
     *
     * @param configKey 参数键名
     * @return 结果
     */
    @GetMapping(value = "/configKey/{configKey}")
    public Result getConfigKey(@PathVariable String configKey) {
        return toAjax(sysConfigService.selectConfigByKey(configKey));
    }
}
