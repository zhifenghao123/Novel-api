package com.novel.kaptcha.exception;

/**
 * 验证码不存在
 *
 * @author novel
 * @date 2019/9/28
 */
public class KaptchaNotFoundException extends KaptchaException {
    public KaptchaNotFoundException() {
        super("Kaptcha Not Found");
    }

    public KaptchaNotFoundException(String message) {
        super(message);
    }
}
