package ${package}.service.impl;

import java.util.List;
import org.springframework.stereotype.Service;
import com.novel.common.utils.StringUtils;
import ${package}.mapper.${className}Mapper;
import ${package}.domain.${className};
import ${package}.service.${className}Service;

/**
 * ${tableComment} 服务层实现
 * 
 * @author ${author}
 * @date ${datetime}
 */
@Service
public class ${className}ServiceImpl implements ${className}Service{

	private final ${className}Mapper ${classname}Mapper;

	public ${className}ServiceImpl(${className}Mapper ${classname}Mapper) {
		this.${classname}Mapper = ${classname}Mapper;
	}

	/**
     * 查询${tableComment}信息
     * 
     * @param ${primaryKey.attrname} ${tableComment}ID
     * @return ${tableComment}信息
     */
	 @Override
	public ${className} select${className}ById(${primaryKey.attrType} ${primaryKey.attrname}){
	    return ${classname}Mapper.select${className}ById(${primaryKey.attrname});
	}
	
	/**
     * 查询${tableComment}列表
     * 
     * @param ${classname} ${tableComment}信息
     * @return ${tableComment}集合
     */
	 @Override
	public List<${className}> select${className}List(${className} ${classname}){
	    return ${classname}Mapper.select${className}List(${classname});
	}
	
    /**
     * 新增${tableComment}
     * 
     * @param ${classname} ${tableComment}信息
     * @return 结果
     */
	 @Override
	public boolean insert${className}(${className} ${classname}){
	    return ${classname}Mapper.insert${className}(${classname}) > 0;
	}
	
	/**
     * 修改${tableComment}
     * 
     * @param ${classname} ${tableComment}信息
     * @return 结果
     */
	 @Override
	public boolean update${className}(${className} ${classname}){
	    return ${classname}Mapper.update${className}(${classname}) > 0;
	}
	
	/**
     * 保存${tableComment}
     * 
     * @param ${classname} ${tableComment}信息
     * @return 结果
     */
	 @Override
	public boolean save${className}(${className} ${classname}){
	    ${primaryKey.attrType} ${primaryKey.attrname} = ${classname}.get${primaryKey.attrName}();
		int rows = 0;
		if (StringUtils.isNotNull(${primaryKey.attrname})){
		    rows = ${classname}Mapper.update${className}(${classname});
		}
		else{
		    rows = ${classname}Mapper.insert${className}(${classname});
		}
		return rows > 0;
	}
	
	/**
     * 删除${tableComment}信息
     * 
     * @param ${primaryKey.attrname} ${tableComment}ID
     * @return 结果
     */
	 @Override
	public boolean delete${className}ById(${primaryKey.attrType} ${primaryKey.attrname}){
	    return ${classname}Mapper.delete${className}ById(${primaryKey.attrname}) > 0;
	}
	
	/**
     * 批量删除${tableComment}对象
     * 
     * @param ${primaryKey.attrname}s 需要删除的数据ID
     * @return 结果
     */
	 @Override
	public boolean batchDelete${className}(${primaryKey.attrType}[] ${primaryKey.attrname}s){
		return ${classname}Mapper.batchDelete${className}(${primaryKey.attrname}s) > 0;
	}
	
}
