package com.novel.common.exception.file;

/**
 * 文件名大小限制异常类
 *
 * @author novel
 * @date 2019/5/24
 */
public class FileSizeLimitExceededException extends FileException {
    private static final long serialVersionUID = 1L;

    public FileSizeLimitExceededException(long defaultMaxSize) {
        super("upload.exceed.maxSize", new Object[]{defaultMaxSize});
    }
}